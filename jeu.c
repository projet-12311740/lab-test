#include <stdlib.h>
#include "jeu.h"

// Fonction pour comparer les choix du joueur et de l'ordinateur
int comparaison(char choixJoueur, char choixOrdinateur) {
    if (choixJoueur == choixOrdinateur) {
        return 0; // Égalité
    } else if ((choixJoueur == 'R' && choixOrdinateur == 'C') ||
               (choixJoueur == 'P' && choixOrdinateur == 'R') ||
               (choixJoueur == 'C' && choixOrdinateur == 'P')) {
        return 1; // Le joueur gagne
    } else {
        return -1; // L'ordinateur gagne
    }
}