CC=gcc
CFLAGS=-std=c99 -Wall

# additional flags for gcov
TESTFLAGS=-fprofile-arcs -ftest-coverage

testcomparaison: testcomparaison.c jeu.h jeu.c
    # build the test
	$(CC) $(CFLAGS) $(TESTFLAGS) -o test testcomparaison.c jeu.c

     # run the test, which will generate like this form {nameOfTest-NameOfFile[.gcna Or .gcno] here will be test-testcomparaison.gcna and test-testcomparaison.gcno
	./test

    # compute how test is covering testcomparaison.
	gcov -c -p test-testcomparaison


clean:
	rm -f *.o test *.gcov *.gcda *.gcno
